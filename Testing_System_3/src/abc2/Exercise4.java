package abc2;

import java.util.Scanner;

public class Exercise4 {
    public static void main(String[] args) {
        Exercise4 exercise4 = new Exercise4();
        exercise4.question1();
    }

    public void question1(){
//        Nhập một xâu kí tự, đếm số lượng các từ trong xâu kí tự đó (các từ có
//                thể cách nhau bằng nhiều khoảng trắng );
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        int length = str.length();
        System.out.println("Số ký tự có khoảng trắng: " + length);
        int length2 = str.replace(" ", "").length();
        System.out.println("Số ký tự ko có khoảng trắng: " + length2);

        // Việt Nam
        String[] danhSachTu = str.split(" ");
        System.out.println("Số từ là: " + danhSachTu.length);
    }

    public void question3(){
//        Viết chương trình để người dùng nhập vào tên và kiểm tra, nếu tên chư
//        viết hoa chữ cái đầu thì viết hoa lên
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        // Áp dụng cho 1 từ
        // viêt -> Việt
        // Lấy ra chữ cái đầu tiên -> uppercase -> thay thế
        String chuCaiDau = str.substring(0,1);
        String newStr = chuCaiDau.toUpperCase() + str.substring(1); // str.substring(1) : Lấy ký tự có index 1 đến hêt || loại đi 1 chữ cái đầu tiên
        // từ 1 + từ 2 + ...

        // Dùng cho tất cả thì dùng hàm for rồi công các newStr lại với nhau
    }
}

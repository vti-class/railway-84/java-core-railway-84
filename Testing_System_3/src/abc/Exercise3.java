package abc;

import abc2.Exercise4;

import java.util.Scanner;

public class Exercise3 {
    public static void main(String[] args) {
        Exercise4 exercise4 = new Exercise4();
        exercise4.question1();

        Exercise1 exercise1 = new Exercise1();
        exercise1.question1();
    }

    public void question1(){
        Scanner scanner = new Scanner(System.in);
        Integer luong = scanner.nextInt();
        float luongConvert = luong;
        System.out.printf("%.2f",luongConvert);
    }

    public void question2(){
//        Khai báo 1 String có value = "1234567"
//        Hãy convert String đó ra số int
        String numStr = "1234567";
        int convert = Integer.parseInt(numStr);
        System.out.println(convert);
    }

    public void question3(){
//        Khai báo 1 String có value = "1234567"
//        Hãy convert String đó ra số int
        Integer number = 1234567;
        int convert = number;
        System.out.println(convert);
    }
}

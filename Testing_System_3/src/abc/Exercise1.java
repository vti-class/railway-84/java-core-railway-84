package abc;

import java.util.Scanner;

public class Exercise1 {
    public static void main(String[] args) {
        Exercise1 exercise1 = new Exercise1();
        exercise1.question1();
    }

    protected void question1(){
        // private < default < protected < public
        Scanner scanner = new Scanner(System.in);
        System.out.println("Mời nhập vào lương 1: ");
        float luong1 = scanner.nextFloat(); // 100,5
        System.out.println("Mời nhập vào lương 2: ");
        float luong2 = scanner.nextFloat();

        // Chuyển đổi kiểu dữ liệu float của lương 1 về int, sau đó in ra
        // (xem làm tròn như nào)
        int luong1convert = (int) luong1;
        int luong2convert = (int) luong2;
        System.out.println("Lương 1 khi làm tròn là: " + luong1convert);
        System.out.println("Lương 2 khi làm tròn là: " + luong2convert);
    }

    public void question2(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Mời bạn nhập vào 1 số có tối đa 5 chữ số:"); // 0 - 99 999
        int number = scanner.nextInt();
        // number -> dạng String -> lenght
        while (number > 99999 || number < 0){
            // vào trong logic khi ng dùng nhập sai, yêu cầu nhập lại ở đây
            System.out.println("Số nhập vào không nằm trong khoảng từ 0 - 99 999. Mời nhập lại: ");
            number = scanner.nextInt();
        }
        // Khi ng dùng nhập đúng, in số đó theo định dạng y/c đề bài
        // (những số dưới 5 chữ số thì sẽ thêm có số 0 ở đầu cho đủ 5 chữ số) 00099
        // Cách 1: convert về String
        String numStr = String.valueOf(number);
        int length = numStr.length();
        System.out.println("0".repeat(5 - length) + numStr); // 00 999

        // Cách 2: sử dụng String fomat hoăc print fomat
        String numStr2  = String.format("%05d", number);
        System.out.println("Cách 2: " + numStr2);

        System.out.printf("%05d", number);

        // Cách 3:
        // 999 / 10 = 99 (1 lần chia) -> soLanLap+ 1
        // 99 / 10 = 9 (2 lần chia vẫn khác 0) -> soLanLap+ 1 = 2
        // 9 / 10 = 0 // Thì thôi, ko cộng thêm nữa
        int soLanLap = 0;
        while (number %10 == 0){
            number = number/10;
            soLanLap++;
        }
        System.out.println("0".repeat(soLanLap) + number);
    }

    public void question3(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Mời bạn nhập vào 1 số có tối đa 5 chữ số:"); // 0 - 99 999
        int number = scanner.nextInt();
        // number -> dạng String -> lenght
        while (number > 99999 || number < 0){
            // vào trong logic khi ng dùng nhập sai, yêu cầu nhập lại ở đây
            System.out.println("Số nhập vào không nằm trong khoảng từ 0 - 99 999. Mời nhập lại: ");
            number = scanner.nextInt();
        }
        // 1234 % 100 = 34
        String numberStr = String.valueOf(number);
        int length = numberStr.length();
        // Lấy ra 2 số cuối
        String kq = numberStr.substring(length -2,length);
        System.out.println(kq);

        // Cách 2:
        int kq2 = number % 100;
        System.out.println("Cách 2: " + kq2);
    }

    public int question4(int a, int b){

        return a/b;
    }
}

public class Exercise6 {
    public static void main(String[] args) {
        Exercise6 exercise6 = new Exercise6();
        exercise6.question3();
    }

    public void question1(){
//        for (int i = 10; i >= 0 ; i=i-2) {
//            System.out.println(i);
//        }
        int number = 10;
        while (number>=0){
            System.out.println(number);
            number = number -2;
        }
    }

    public void question2(Account account){
        System.out.println(account.accountId);
        System.out.println(account.fullName);
        // ...
    }

    public void question3(){
        for (int i = 10; i >= 0 ; i--) {
            System.out.println(i);
        }
    }
}

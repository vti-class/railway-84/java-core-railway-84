public class Exercise1 {

    public void question1(Account account2){
        System.out.println("Chạy bài 1");
        // Kiểm tra account thứ 2
        //Nếu không có phòng ban (tức là department == null) thì sẽ in ra text
        //"Nhân viên này chưa có phòng ban"
        //Nếu không thì sẽ in ra text "Phòng ban của nhân viên này là ..."

        // logic làm bài 1
        if (account2.department == null){
            System.out.println("Nhân viên này chưa có phòng ban");
        } else {
            System.out.println("Phòng ban của nhân viên này là: " + account2.department.departmentName);
        }

    }

    public void question2(Account account2){
        System.out.println("Chạy bài 2");
        int size = account2.groups.length;
        if (size==0){
            System.out.println("Nhân viên này chưa có group");
        } else if (size==1 || size==2){
            System.out.println("Group của nhân viên này là Java Fresher, C# Fresher");
        } else if(size == 3){
            System.out.println("Nhân viên này là người quan trọng, tham gia nhiều group ");
        } else if (size == 4){
            System.out.println("Nhân viên này là người hóng chuyện, tham gia tất cả các group");
        } else {
            System.out.println("Các trường hợp còn lại");
        }
    }

    public void question3(Account account2){
        System.out.println("Chạy bài 3");
        System.out.println((account2.department == null) ? "Nhân viên này chưa có phòng ban"
                : "Phòng ban của nhân viên này là: " + account2.department.departmentName);
    }

    public void question5(Account[] accounts){
        int size = accounts.length;
        switch (size){
            case 1:
                System.out.println("Nhóm có một thành viên");
                break;
            case 3:
                System.out.println("Nhóm có 3 thành viên");
                break;
            case 2:
                System.out.println("Nhóm có 2 thành viên");
                break;
            default:
                System.out.println("Nhóm có nhiều thành viên");
        }
    }

    public void question17(Account[] accounts){
        // Question 10
//        int size = accounts.length;
//        do {
//            Account account = accounts[size-1];
//            // IN theo dung dinh dang yeu cau cua bai
//            System.out.println(account.accountId);
//            // In ra cac thong tin khac
//            size--;
//        }while (size>=1);

        // Question 12:
        // Chỉ in ra thông tin 2 account đầu tiên theo định dạng như Question 10
        int size = accounts.length;
        int i = 0;
        do {
            Account account = accounts[i];
 //         IN theo dung dinh dang yeu cau cua bai
            System.out.println(account.accountId);
            // In ra cac thong tin khac
            i++;
            if (i == 2){
                break;
            }
        } while (i<size);


    }

    public void question15(){
        // In ra các số chẵn nhỏ hơn hoặc bằng 20
        //C1:
//        for (int i = 0; i <= 20; i=i+2) {
//            System.out.println(i);
//        }

        //C2:
        for (int i = 0; i <= 20; i++) {
            if (i%2!=0){
                continue; // Neu ko phai so chan -> di den dk tiep theo
            }
            System.out.println(i);
        }
    }


}

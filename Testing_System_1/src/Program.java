public class Program {
    public static void main(String[] args) {
        //-------------Insert dữ liệu INSERT Department --------------
        Department department1 = new Department();
        department1.departmentId = 1;
        department1.departmentName = "Phòng ban 1";

        Department department2 = new Department();
        department2.departmentId = 2;
        department2.departmentName = "Phòng ban 2";

        Department department3 = new Department();
        department3.departmentId = 3;
        department3.departmentName = "Phòng ban 2";

        //-------------Insert dữ liệu INSERT Group --------------
        Group group1 = new Group();
        Group group2 = new Group();
        Group group3 = new Group();

        Group[] dsGroup1 = {group1, group2, group3};

        //-------------Insert dữ liệu INSERT Account --------------
        Account account1 = new Account(); // 1 contructor default, hàm khởi tạp không tham số
//        account1.accountId = 1;

        Account account2 = new Account();
//        account2.accountId = 2;
        account2.department = department2;
        account2.groups = dsGroup1;

        Account account3 = new Account();
//        account3.accountId = 3;

        System.out.println(account1);
        System.out.println(account3);

        Account[] accounts1 = {account1, account2, account3};
        // In ra tên từng account ID trong danh sách Account accounts1
//        int length = accounts1.length;
//        for (int i = 0; i < accounts1.length; i++) {
//            System.out.println("ID : " + accounts1[i].accountId);
//        }

        // Sử dụng for each
        // Đối tượng sử dụng: là 1 Array
//        for(Account account : accounts1){
//            System.out.println("ID Cách 2: " + account.accountId);
//            System.out.println("Email Cách 2: " + account.email);
//        }

//        int size = accounts1.length; // 3
//        int i = 0;
//        while (i < size){
//            System.out.println("ID Cách 3: " + accounts1[i].accountId);
//            i++;
//        }


        // Làm bài tập Exercise1
        Exercise1 exercise1 = new Exercise1();
//        exercise1.question2(account2);
//        exercise1.question1(account2);
//        exercise1.question3(account2);
//        exercise1.question5(accounts1);
//        exercise1.question17(accounts1);
    }
}

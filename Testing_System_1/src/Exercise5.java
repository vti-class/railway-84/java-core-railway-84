import java.util.Date;
import java.util.Scanner;

public class Exercise5 {
    public static void main(String[] args) {
        Exercise5 exercise5 = new Exercise5();
        exercise5.question7();
    }

//    Question 1:
//    Viết lệnh cho phép người dùng nhập 3 số nguyên vào chương trình
    public void question1(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Mời b nhập vào số thứ 1: ");
        int number1 = scanner.nextInt();
        System.out.println("Mời b nhập vào số thứ 2: ");
        int number2 = scanner.nextInt();
        System.out.println("Mời b nhập vào số thứ 3: ");
        int number3 = scanner.nextInt();
        System.out.println("3 số vừa nhập vào là: " + number1 + "; " +
                number2 + "; " + number3);
    }

//    Question 2:
//    Viết lệnh cho phép người dùng nhập 2 số thực vào chương trình
    public void question2(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Mời b nhập vào số thực thứ 1: ");
        float number1 = scanner.nextFloat();
        System.out.println("Mời b nhập vào số thực thứ 2: ");
        float number2 = scanner.nextFloat();
        System.out.println("2 số vừa nhập vào là: " + number1 + "; " +
                number2 + "; ");
    }

    public void question3(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Mời nhập vào họ và tên");
        String fullName = scanner.nextLine();
        System.out.println("Họ tên đầy đủ là: " + fullName);
    }

    public void question4(){
        Scanner scanner = new Scanner(System.in);
        String date = scanner.nextLine();//dd-MM-yyyy (02-02-2000)
        Date date1 = new Date(date);
    }

    public void question5() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Mời bạn chọn phòng ban: ");
        System.out.println("1. DEV");
        System.out.println("2. TEST");
        PositionName positionName;
        int choose = scanner.nextInt();
        switch (choose){
            case 1: positionName = PositionName.Dev;
            break;
            case 2: positionName = PositionName.Test;
            break;
            default:
                positionName = PositionName.Dev;
        }
        System.out.println("Phòng ban bạn vừa chọn là: " + positionName.name());

        Department department = new Department();
        int departID = scanner.nextInt();
        String departmentName = scanner.nextLine();

        department.departmentId = departID;
        department.departmentName = departmentName;
    }

    public void question7(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Mời nhập vào số chẵn");
        int numberChan = scanner.nextInt();
        while (numberChan%2 != 0){
            System.out.println("Số vừa nhập ko phải số chẵn, mời nhập lại: ");
            numberChan = scanner.nextInt();
        }
        System.out.println("Số chẵn vừa nhập vào là: " + numberChan);
    }
}

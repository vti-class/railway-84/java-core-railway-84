import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Exercise3 {
    public static void main(String[] args) {
        Exercise3 exercise3 = new Exercise3();
        exercise3.ques3();;
    }

    public void ques1(){
        Date date = new Date();
        Locale locale = new Locale("vi");
        String p1 = "MMM dd, yyy 'hiat' h:mm a";
        SimpleDateFormat dateFormat = new SimpleDateFormat(p1, locale);
        System.out.println(dateFormat.format(date));

    }

//    Question 3:
//    Chỉ in ra năm của create date property trong Question 2
    public void ques3(){
        Date date = new Date();
        String format = "yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        System.out.println(simpleDateFormat.format(date));
    }

    public void ques4(){
        Date date = new Date();
        String format = "MM-yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        System.out.println(simpleDateFormat.format(date));
    }
}

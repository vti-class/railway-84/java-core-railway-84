public class DemoIfElse {
    public static void main(String[] args) {
        int a = 8;
        int b = 6;
//        System.out.println(a<b);
//        if (a < b){
//            System.out.println("Trường hợp a < b");
//        } else {
//            System.out.println("Các trường hợp còn lại");
//        }

        String message;
//        if (a < b){
//            message = "Trường hợp a < b";
//        } else {
//            message ="Các trường hợp còn lại";
//        }
//        message = (a < b) ? "Trường hợp a < b" : "Các trường hợp còn lại";
//        System.out.println(message);

        System.out.println((a < b) ? "Trường hợp a < b" : "Các trường hợp còn lại");
    }
}

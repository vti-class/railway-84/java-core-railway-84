// Java là 1 ngôn ngữ lập trình hướng đối tượng(có đặc điểm và có hành động)
// các đặc điểm... : Là thuộc tính trong 1 class Property
// Các hành động là các phương thức(method, hàm)
//  - hàm main: dùng để chạy chương trình
//  - và các mehtod thường
public class Program {


    // public: khả năng truy cập của class: public mức độ truy cập cao nhất, toàn project
    // có thể gọi được đến nó
    public static void main(String[] args) {
       // Nội dung code
        int number = 3;
        //1. Kiểu dữ liệu nguyên thuỷ (primitive))
        //   - Kiểu dữ liệu dạng logic: boolean( true; false)
        boolean logic = false;
        //   - Kiểu dữ liệu dạng ký tự, char (giá trị sẽ là 1 ký tự trên bàn phím):
        char type3 = 'h';
        //   - Kiểu dữ liệu số nguyên: byte, short, int, long
        byte number1 = 110; // chiếm 1 byte
        short number2 = 5; // 2byte
        int number3 = 10; // 4byte
        long number4 = 2000; // 8byte

        int number5 = 15;
        int number6 = number3 + number5;
        int number7 = type3 + 5;
        // Kiểu dữ liệu dạng số thực: float, double:
        float number8 = 7.8f;  //4byte
        double number9 = 7.8; // 8byte

        // 8 kiểu dữ liệu nguyên thuỷ (primitive)



        String chuoi1 = "Hello";
        System.out.println(chuoi1);
    }

    // Method thường:
    public void abc(){

    }
}

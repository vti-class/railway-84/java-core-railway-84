public class DemoDataCasting {
    public static void main(String[] args) {
//        int a = 10;
//        long b = a; // Ép kiểu dữ liệu của a thành long


        long a = 10;
        int b = (int) a; // Ép kiểu dữ liệu của a thành int, nhỏ hơn kiểu dữ liệu ban đầu
    }
}

import java.util.Scanner;

public class DemoInput {
    public static void main(String[] args) {
        DemoInput demoInput = new DemoInput();


        // Muốn nhập vào từ màn hình console các giá trị để gán vào 1 biến trong java
        Scanner scanner = new Scanner(System.in); // Cú pháp để khai báo nhập 1 giá trị từ console

        String text = scanner.nextLine(); // Lấy 1 giá trị có kiểu dữ liệu là String ở console
        System.out.println(text);

//        int number1 = scanner.nextInt();// Lấy 1 giá trị có kiểu dữ liệu là int ở console
//        System.out.println(number1);


//        int number1 = scanner.nextInt();// Lấy 1 giá trị có kiểu dữ liệu là int ở console
//        int number2 = scanner.nextInt();// giá trị được nhập từ console
//        int kq = demoInput.phepCong(number1, number2);
//        System.out.println("Kết quả phép cộng 2 số là: " + kq);
    }

    public int phepCong(int a, int b){
        return a + b;
    }
}

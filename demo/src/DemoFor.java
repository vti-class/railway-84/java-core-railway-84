public class DemoFor {
    public static void main(String[] args) {
        // In ra các số từ 1-> 1000

        // tạo biến i, có gía trị bắt đầu = 1
        // Sau mỗi lần in -> cộng thêm 1 đơn vị cho biến i
        // In tiếp ra i tới khi nào i có giá trị = 1000 thì dừng

//        for (int i = 1; i <= 10; i=i+2) {
//            System.out.println(i);
//        }

        //         for ( -- Giá trị ban đầu của i --; - Điều kiện đề dừng vòng lặp - ; -Bước nhảy sau mỗi vòng lặp-) {
        //            Xử lý logic khi có gí trị i
        //        }

        // In ra các số chẵn từ 1->10
        for (int i = 2; i <= 10; i+=2) {
            System.out.println(i);
        }
    }
}

public class DemoToanTuLoGic {
    public static void main(String[] args) {
//        int a = 5;
//        int b = 9;
//        int c = 7;



        int i = 8;
        int a = ++i; // a = 9
        int b = ++a; // b = 10; a = 10
        int c = b--; // c = 10
// c = 10, b = 9, a = 10
        System.out.println(c > a && c > b); // -> false

        System.out.println(c > a || c > b); // -> true


        // Điều kiện: c > a: đúng; và c > b: đúng
//        if (c > a && c > b){ // cả 2 biểu thức đúng thì điều kiện sẽ là true
//            System.out.println("c lớn hơn cả a và b");
//        }
    }
}

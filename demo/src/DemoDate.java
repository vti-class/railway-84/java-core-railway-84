import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DemoDate {
    public static void main(String[] args) throws FileNotFoundException {
        Date date1 = new Date();
        System.out.println(date1);
        // In ngày theo định dạng dd/MM/yyyy hh:mm:ss
        // M: Month, minutes
        String patten = "dd-MM-yy hh:mm:ss";
        SimpleDateFormat formatter = new SimpleDateFormat(patten);
        String kq = formatter.format(date1);
        System.out.println(kq);
    }
}

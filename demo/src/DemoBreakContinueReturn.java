public class DemoBreakContinueReturn {
    public static void main(String[] args) {

        for (int j = 0; j < 3; j++) {
            System.out.println("J là: " + j);
            if (j == 1) {
                break;
            }
        }

        // Khu vực ngoài vòng for
        System.out.println("Đã thoát ra khỏi vòng lặp J");
    }
}

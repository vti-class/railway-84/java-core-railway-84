public class DemoSwitchCase {
    public static void main(String[] args) {
        int number = 1;
        // Nếu number = 1 -> thực hiện chức năng 1
        // Nếu number = 2 -> thực hiện chức năng 2
        //...
        // Nếu number = 3 -> thực hiện chức năng 2
        switch (number){
            case 1:
                System.out.println("chức năng 1");
//                break;
            case 2:
                System.out.println("chức năng 2");
//                break;
            case 3:
                System.out.println("chức năng 3");
                break;
            case 4:
                System.out.println("chức năng 4");
                break;
            default:
                System.out.println("chức năng còn lại");
        }
    }
}

public class DemoArray {
    public static void main(String[] args) {
        // Khai báo 1 danh sách số int
        int[] numbers = {1, 4, 5};
        //System.out.println(numbers[0]); // In ra số ở vị trí thứ 0
        //System.out.println(numbers.length); // In ra số phần tử có trong dánh sách

        float[] numbers2 = new float[5]; // Tạo 1 array trống có thể chứa tối đa 5 phần tử
        numbers2[0] = 1.1f; // Gán giá trị cho các ô trống đó
        System.out.println(numbers2[4]);
    }
}

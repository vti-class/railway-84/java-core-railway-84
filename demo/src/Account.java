import java.util.Date;

public class Account {
    int accountId;// public;default,protected;private
    String email;
    String userName;
    String fullName;
    int positionId;
    Date createDate;

    @Override
    public String toString() {
        return "accountId: " + accountId + "; email: " + email;
    }
}

public class DemoWhileTrue {
    public static void main(String[] args) {
        int i = 1;
//        while (i<=10){ // Trong ngoặc: Điều kiện để đi vào logic
//            // Thực hiện logic
//            System.out.println(i);
//            i++;
//        }

        do {
            System.out.println(i);
            i++;
        } while (i<=10);
    }
}

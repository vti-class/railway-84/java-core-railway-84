public class WrapperClass {
    public static void main(String[] args) {
        boolean a = false;
        Boolean b = a; // Ép kiểu a từ nguyên thuỷ sang dạng Object (WrapperClass) -- C1
        Boolean c = Boolean.valueOf(a); // -- C2
        // --> ép kiểu như này gọi là boxing


        Boolean a2 = true; // a2 có kiểu dữ liệu là Boolean
        boolean b2 = a2;  // C1: ép kiểu a2 từ Boolean -> boolean
        // --> un boxing


        // Ép kiểu từ String -> int
        int number1 = Integer.parseInt("121212av");
        System.out.println(number1);
    }
}

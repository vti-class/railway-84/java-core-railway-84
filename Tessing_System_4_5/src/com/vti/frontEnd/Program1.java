package com.vti.frontEnd;

import com.vti.backend.Exercise1;

public class Program1 {
    public static void main(String[] args) {
        Exercise1 exercise1 = new Exercise1();
        // Khi chạy hàm main, chương trình sẽ load, nạp những biến,
        // method,... được khai báo là static
        // các giátrij của biến, method sẽ tồn tại đến khi chương trình kết thúc
        // Vì vậy, mình có thể gọi trực tiếp biến, method.. static mà ko qua
        // hàm khởi tạo
        while (true){
            System.out.println("Mời bạn chọn bài tập: ");
            System.out.println("1. Exercise 1 question 1");
            System.out.println("2. Exercise 1 question 2");
            System.out.println("3. Exercise 1 question 3");
            System.out.println("4. Thoát chương trình");
            int number = ScannerUtils.inputNumber(1, 4);
            // Tạo 1 method để nhập vào màn hình 1 số từ a cho tới b
            // Các class mà có chức năng sử dụng đi sử dụng lại nhiều lần: ScannerUtils, StringUtils, DateUtils,...
            // -> tạo ra class Utils, các method để là static
            switch (number){
                case 1:
                    Exercise1.question1();
                    break;
                case 2:
                    exercise1.question2();
                    break;
                case 3:
                    exercise1.question3();
                    break;
                case 4:
                    return;
            }
        }
    }
}

package com.vti.entity;

import java.util.Date;

public class Account {
    public static int number = 1;
    int accountId;// ko muốn mọi chỗ có thể thay đổi được giá trị của id
    String email;
    String userName;
    String fullName;
    Department department;
    int positionId;
    Date createDate;

    Group[] groups;
    // 100 thuộc tính nữa

    public Account(int id, String email){
        this.accountId = Account.number;
        this.email = email;
        Account.number = Account.number++;
    }

    public Account(int accountId, String email, String userName, String fullName) {
        this.accountId = Account.number;
        Account.number = ++Account.number;
        this.email = email;
        this.userName = userName;
        this.fullName = fullName;
    }

    public Account() {
        this.accountId = Account.number;
        Account.number = ++Account.number;
    }

    @Override
    public String toString() {
        return "Account{" +
                "accountId=" + accountId +
                "; email="+ email +
                '}';
    }
}
